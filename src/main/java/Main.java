import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class Main {

    private static String decryptMessage(Stream<Character> encryptedMessage) {
        return "--encrypted--";
    }

    private static String getPassword(Stream<Path> filesStream) throws IOException {
        return "--encrypted--";
    }

    public static void main(String[] args) throws IOException {

        Stream<Character> encryptedMessage = new String(Files.readAllBytes(Paths.get("README.txt"))).chars().mapToObj(value -> (char) value);
        System.out.println(decryptMessage(encryptedMessage));

//        System.out.println();
//        Stream<Path> filesStream = Files.walk(Paths.get("KENYOOSEE-PC\\"));
//        System.out.println(getPassword(filesStream));
    }
}
